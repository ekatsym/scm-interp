(defpackage scm-interp.environment
  (:use :cl
        :scm-interp.util
        :scm-interp.alist)

  (:import-from :optima
                #:match)

  (:import-from :alexandria
                #:with-gensyms
                #:once-only
                #:hash-table-alist)

  (:export #:*primitive-environment*
           #:scheme-symbols

           #:lookup
           #:make-new-frame

           #:value
           #:syntax
           #:macro

           #:true
           #:false

           #:scheme-unbound-variable-error
           #:scheme-no-frame-error))
(in-package :scm-interp.environment)


;;; main
(defparameter *primitive-environment*
  (list (make-hash-table :test #'eq)))
(defun scheme-symbols (env)
  (mapcan (compose (partial #'mapcar #'car)
                   #'hash-table-alist)
          env))

(defstruct scheme-symbol
  value tag)

(defun lookup (var env)
  (declare (optimize speed))
  (match env
    ((cons frame1 frames)
     (match (gethash var frame1)
       ((scheme-symbol :value val :tag tag)
        (values val tag))
       (()
        (lookup var frames))))
    (()
     (error 'scheme-unbound-variable-error
            :variable var))))

(defun make-new-frame (env)
  (cons (make-hash-table :test #'eq) env))

(defun %bind! (var env val tag)
  (if (null env)
      (error 'scheme-no-frame-error)
      (match env
        ((cons frame1 _)
         (setf (gethash var frame1)
               (make-scheme-symbol
                 :value val
                 :tag tag))))))

(defmacro bind! (var env val-tag)
  (with-gensyms (val tag)
    `(multiple-value-bind (,val ,tag) ,val-tag
       (%bind! ,var ,env ,val (or ,tag 'value)))))

(defsetf lookup bind!)


;;; condition
(define-condition scheme-unbound-variable-error (error)
  ((variable :initarg :variable
             :accessor scheme-unbound-variable-error-variable))
  (:report
    (lambda (o s)
      (format s "~&The variable ~s is unbound."
              (scheme-unbound-variable-error-variable o)))))

(define-condition scheme-no-frame-error (error)
  ((environment :initarg :environment
                :accessor scheme-no-frame-error-environment))
  (:report
    (lambda (o s)
      (format s "~&There are no frames in the environment,~%~4t~s~%"
              (scheme-no-frame-error-environment o)))))


;;; boolean
(setf (lookup 'true *primitive-environment*)
      (values 'true 'value))
(setf (lookup 'false *primitive-environment*)
      (values 'false 'value))


;;; alist version
#|
(defparameter *primitive-environment* '(()))
(defun scheme-symbols (env)
  (mapcan (partial #'mapcar #'car) env))

(defstruct scheme-symbol
  value tag)

(defun lookup (var env)
  (match env
    ((cons frame1 frames)
     (match (aget var frame1)
       ((scheme-symbol :value val :tag tag)
        (values val tag))
       (()
        (lookup var frames))))
    (()
     (error 'scheme-unbound-variable-error
            :variable var))))


(defun make-new-frame (env)
  (cons '() env))

(defun %bind! (var env val tag)
  (if (null env)
      (error 'scheme-no-frame-error
             :environment env)
      (setf (first env)
            (aset var (first env)
                  (make-scheme-symbol :value val
                                      :tag tag)))))

(defmacro bind! (var env val-tag)
  (with-gensyms (val tag)
    `(multiple-value-bind (,val ,tag) ,val-tag
       (%bind! ,var ,env ,val (or ,tag 'value)))))

(defsetf lookup bind!)
|#
