;;; tail-recursion version
(defun scheme-eval (expr env cont)
  (TAGBODY
    TOP
    (cond
      ((self? expr)
       (RETURN-FROM SCHEME-EVAL (funcall cont expr)))

      ((variable? expr)
       (RETURN-FROM SCHEME-EVAL (funcall cont (lookup expr env))))

      ((application? expr)
       (let ((oper (first expr))
             (args (rest expr)))
         (cond
           ((special-form? oper)
            (ecase oper
              (quote
                (match args
                  ((list datum)
                   (RETURN-FROM SCHEME-EVAL (funcall cont datum)))
                  (_ (error 'scheme-arity-error
                            :operator 'quote
                            :expected 1
                            :given (length args)))))

              (if
                 (match args
                   ((list test then else)
                    (if (not (eq 'false (scheme-eval test env #'values)))
                        (setf expr then)
                        (setf expr else))
                    (GO TOP))
                   (_ (error 'scheme-arity-error
                             :operator 'if
                             :expected 3
                             :given (length args)))))

              (begin
                (match args
                  ((list body1)
                   (setf expr body1)
                   (GO TOP))
                  ((cons body1 rest-body)
                   (scheme-eval body1 env #'values)
                   (setf expr `(begin ,@rest-body))
                   (GO TOP))))

              (lambda
                (match args
                  ((cons fomals body)
                   (let ((cl-lambda-list
                           (cond ((null fomals)
                                  '())
                                 ((atom fomals)
                                  `(&rest ,fomals))
                                 ((proper-list-p fomals)
                                  fomals)
                                 ((dotted-list-p fomals)
                                  `(,@(butlast fomals) ,(car (last fomals)) &rest ,(cdr (last fomals))))
                                 (t
                                  (error 'scheme-lambda-fomals-error
                                         :fomals fomals))))
                         (body (if (single body) (first body) `(begin ,@body)))
                         (new-env (gensym "NEW-ENV")))
                     (setf expr (eval
                                  `(lambda ,cl-lambda-list
                                     (let ((,new-env (make-new-frame ',env)))
                                       ,@(mapcar
                                           (lambda (arg)
                                             `(setf (lookup ',arg ,new-env)
                                                    ,arg))
                                           (remove '&rest cl-lambda-list))
                                       (scheme-eval ',body
                                                    ,new-env
                                                    #'values)))))
                     (GO TOP)))))

              (define
                (match args
                  ((cons (cons name fomals) body)
                   (setf expr `(define ,name (lambda ,fomals ,@body)))
                   (GO TOP))
                  ((list name body)
                   (setf (lookup name env)
                         (scheme-eval body env #'values))
                   (RETURN-FROM SCHEME-EVAL name))
                  (_ (error 'scheme-arity-error
                            :operator 'define
                            :expected '(2 or more than)
                            :given (length args)))))

              (set!
                (match args
                  ((list name body)
                   (lookup name env)
                   (setf (lookup name env)
                         (scheme-eval body env #'values))
                   (RETURN-FROM SCHEME-EVAL name))
                  (_ (error 'scheme-arity-error
                            :operator 'set!
                            :expected 2
                            :given (length args)))))))

           ((value? oper env)
            (let ((old-cont cont))
              (setf expr oper
                    cont (lambda (proc)
                           (eval-args args
                                      env
                                      (lambda (evaled-args)
                                        (funcall old-cont (apply proc evaled-args)))))))
            (GO TOP))

           ((macro? oper env)
            (setf expr (funcall (lookup oper env) args env cont))
            (GO TOP)))))

      (t
        (error 'scheme-eval-error
               :expression expr
               :environment env
               :continuation cont)))))

