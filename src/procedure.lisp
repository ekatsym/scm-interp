(defpackage scm-interp.procedure
  (:use :cl
        :scm-interp.util
        :scm-interp.core
        :scm-interp.syntax
        )
  (:export #:eqv?
           #:eq?
           #:equal?

           ;; number
           #:number?  #:complex?  #:real?  #:rational?  #:integer?  #:exact?  #:inexact?
           #:= #:< #:> #:<= #:>=
           #:zero?  #:positive?  #:negarive?  #:odd?  #:even?
           #:max #:min
           #:+ #:* #:- #:/
           #:abs
           #:quotient #:remainder #:modulo
           #:gcd #:lcm
           #:numerator #:denominator
           #:floor #:ceiling #:truncate #:round
           #:rationalize
           #:exp #:log
           #:sin #:cos #:tan #:asin #:acos #:atan 
           #:sqrt #:expt 
           #:make-rectangular
           #:make-polar #:real-part #:imag-part #:magnitude #:angle 
           #:exact->inexact #:inexact->exact 
           #:number->string #:string->number

           ;; boolean
           #:not
           #:boolean?

           ;; pair and list
           #:pair?
           #:cons #:car #:cdr
           #:set-car!  #:set-cdr!
           #:caar #:cadr #:cdar #:cddr #:caaar #:caadr #:cadar #:caddr #:cdaar #:cdadr #:cddar #:cdddr
           #:caaaar #:caaadr #:caadar #:caaddr #:cadaar #:cadadr #:caddar #:cadddr #:cdaaar
           #:cdaadr #:cdadar #:cdaddr #:cddaar #:cddadr #:cdddar #:cddddr
           #:null?  #:list?
           #:list
           #:length #:append #:reverse
           #:list-tail #:list-ref
           #:memq #:memv #:member
           #:assq #:assv #:assoc

           ;; symbol
           #:symbol?
           #:string->symbol

           ;; character
           #:char?
           #:char=?  #:char<?  #:char>?  #:char<=?  #:char>=?
           #:char-ci=?  #:char-ci<?  #:char-ci>?  #:char-ci<=?  #:char-ci>=?
           #:char-alphabetic?  #:char-numeric?  #:char-whitespace?  #:char-upper-case?  #:char-lower-case?
           #:char->integer #:integer->char
           #:char-upcase #:char-downcase

           ;; string
           #:string?  #:make-string
           #:string
           #:string-length #:string-ref #:string-set!
           #:string=?  #:string<?  #:string>?  #:string<=?  #:string>=?
           #:string-ci=?  #:string-ci<?  #:string-ci>?  #:string-ci<=?  #:string-ci>=?
           #:substring
           #:string-append
           #:string->list #:list->string
           #:string-copy
           #:string-fill!

           ;; vector
           #:vector?  #:make-vector
           #:vector
           #:vector-length #:vector-ref #:vector-set!
           #:vector->list #:list->vector
           #:vector-fill!

           ;; procedure
           #:procedure?
           #:apply
           #:map
           #:force
           #:call-with-current-continuation
           #:values
           #:call-with-values
           #:dynamic-wind

           ;; eval
           #:eval
           #:scheme-report-environment
           #:null-environment

           ;; port
           #:call-with-input-file
           #:call-with-output-file
           #:input-port?
           #:output-port?
           #:current-input-port
           #:current-output-port
           #:open-input-file
           #:open-output-file
           #:close-input-file
           #:close-output-file

           ;; input
           #:read
           #:read-char
           #:peek-char
           #:eof-object?
           #:char-ready?

           ;; output
           #:write
           #:display
           #:newline
           #:write-char

           ;; system interface
           #:load
           #:transcript-on))
(in-package :scm-interp.procedure)



