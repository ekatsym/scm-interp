(defpackage scm-interp.alist
  (:use :cl
        :scm-interp.util)
  (:import-from :alexandria
                #:with-gensyms
                #:once-only)
  (:import-from :optima
                #:match
                #:ematch
                #:defpattern)
  (:export #:associative-list
           #:associative-list-p
           #:alist
           #:aget
           #:aset
           #:aset!
           #:arem))
(in-package :scm-interp.alist)


;;; main
(defun associative-list-p (object)
  (or (null object)
      (and (consp object)
           (consp (car object))
           (associative-list-p (cdr object)))))

(deftype associative-list ()
  `(satisfies associative-list-p))

(defun alist (&rest aconss)
  (if (null aconss)
      '()
      (acons (first aconss) (second aconss)
             (apply #'alist (nthcdr 2 aconss)))))

(defun aget (key alist)
  (cond ((null alist)
         nil)
        ((eql key (caar alist))
         (cdar alist))
        (t
         (aget key (cdr alist)))))

(defun aset (key alist datum)
  (labels ((rec (alst acc)
             (cond ((null alst)
                    (acons key datum acc))
                   ((eql key (caar alst))
                    (revappend (cdr alst) (acons key datum acc)))
                   (t
                    (rec (cdr alst) (cons (car alst) acc))))))
    (rec alist '())))

(defmacro aset! (key alist datum)
  `(setf ,alist
         (aset ,key ,alist ,datum)))

(defun arem (key alist)
  (cond ((null alist)
         '())
        ((eql key (caar alist))
         (cdr alist))
        (t
         (acons (caar alist) (cdar alist)
                (arem key (cdr alist))))))


;;; pattern
(defpattern acons (key datum alist)
  `(cons (cons ,key ,datum) ,alist))

(defpattern alist (&rest aconss)
  (if (null aconss)
      nil
      `(acons ,(first aconss) ,(second aconss)
              (alist ,@(nthcdr 2 aconss)))))
