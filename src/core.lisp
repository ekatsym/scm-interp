(defpackage scm-interp.core
  (:use :cl
        :scm-interp.util
        :scm-interp.environment)

  (:import-from :alexandria
                #:with-gensyms)

  (:import-from :optima
                #:match
                #:ematch)

  (:export #:*primitive-environment*

           #:lookup
           #:bind

           #:value
           #:syntax
           #:macro

           #:true
           #:false

           #:scheme-eval

           #:quote
           #:if
           #:begin
           #:lambda
           #:define
           #:set!

           #:define-scheme-syntax
           #:define-scheme-macro
           #:define-scheme-procedure

           #:scheme-arity-error
           #:scheme-unbound-variable-error
           #:scheme-no-frame-error

           #:with-scheme))
(in-package :scm-interp.core)


(defun self? (expr)
  (or (null expr)
      (eq 'true expr)
      (eq 'false expr)
      (numberp expr)
      (characterp expr)
      (stringp expr)
      (vectorp expr)
      (streamp expr)
      (functionp expr)))

(defun variable? (expr)
  (symbolp expr))

(defun application? (expr)
  (consp expr))

(defun syntax? (var env)
  (and (variable? var)
       (multiple-value-bind (val tag) (lookup var env)
         (declare (ignore val))
         (eq tag 'syntax))))

(defun special-form? (var)
  (member var '(quote if begin lambda define set!)))

(defun macro? (var env)
  (and (variable? var)
       (multiple-value-bind (val tag) (lookup var env)
         (declare (ignore val))
         (eq tag 'macro))))

(defun value? (x env)
  (or (application? x)
      (and (variable? x)
           (multiple-value-bind (val tag) (lookup x env)
             (declare (ignore val))
             (eq tag 'value)))))


;;; helper
(defun eval-args (args env cont)
  (declare (optimize speed)
           (type function cont))
  (if (null args)
      (funcall cont '())
      (scheme-eval (first args)
                   env
                   (lambda (arg1)
                     (eval-args (rest args)
                                env
                                (lambda (rest-args)
                                  (funcall cont (cons arg1 rest-args))))))))


;;; main
(defun scheme-eval (expr env cont)
  (declare (optimize speed)
           (type function cont))
  (cond ((self? expr)
         (funcall cont expr))
        ((variable? expr)
         (funcall cont (lookup expr env)))
        ((application? expr)
         (scheme-apply (first expr) (rest expr) env cont))
        (t
         (error 'scheme-eval-error
                :expression expr
                :environment env
                :continuation cont))))

(defun scheme-apply (oper args env cont)
  (declare (optimize speed)
           (type function cont))
  (cond ((value? oper env)
         (scheme-eval oper
                      env
                      (lambda (cl-fn)
                        (declare (type function cl-fn))
                        (eval-args args
                                   env
                                   (lambda (evaled-args)
                                     (funcall cont
                                              (apply cl-fn evaled-args)))))))
        ((syntax? oper env)
         (let ((syntax (lookup oper env)))
           (declare (type function syntax))
           (funcall syntax args env cont)))
        ((macro? oper env)
         (let ((macro (lookup oper env)))
           (declare (type function macro))
           (scheme-eval (funcall macro args env cont) env cont)))))


;;; scheme-value or syntax
(defmacro define-scheme-syntax (name (args env cont) &body body)
  (with-gensyms (args-sym)
    `(setf (lookup ',name *primitive-environment*)
           (values (lambda (,args-sym ,env ,cont)
                     (declare (ignorable ,env ,cont))
                     (destructuring-bind ,args ,args-sym
                       (declare (ignorable ,@(remove '&rest args)))
                       ,@body))
                   'syntax))))

(defmacro define-scheme-macro (name args &body body)
  (with-gensyms (env cont args-sym)
    `(setf (lookup ',name *primitive-environment*)
           (values (lambda (,args-sym ,env ,cont)
                     (declare (ignorable ,env ,cont))
                     (destructuring-bind ,args ,args-sym
                       (declare (ignorable ,@(remove '&rest args)))
                       ,@body))
                   'macro))))

(defmacro define-scheme-procedure (name args &body body)
  `(setf (lookup ',name *primitive-environment*)
         (values (lambda ,args ,@body)
                 'value)))



;;; condition
(define-condition scheme-eval-error (error)
  ((expression :initarg :expression
               :accessor scheme-eval-error-expression)
   (environment :initarg :environment
                :accessor scheme-eval-error-environment)
   (continuation :initarg :continuation
                 :accessor scheme-eval-error-continuation))
  (:report
    (lambda (o s)
      (format s
              "~&Given expression is invalid.~%~4tEXPRESSION: ~s~%~4tENVIRONMENT: ~s~%~4tCONTINUATION: ~s~%"
              (scheme-eval-error-expression o)
              (scheme-eval-error-environment o)
              (scheme-eval-error-continuation o)))))

(define-condition scheme-arity-error (error)
  ((expected :initarg :expected
             :accessor scheme-arity-error-expected)
   (operator :initarg :operator
             :accessor scheme-arity-error-operator)
   (given :initarg :given
          :accessor scheme-arity-error-given))
  (:report
    (lambda (o s)
      (format s "~s expected expression number is ~s, but given ~s."
              (scheme-arity-error-operator o)
              (scheme-arity-error-expected o)
              (scheme-arity-error-given o)))))


;;; with-scheme1
(defmacro with-scheme (env &body body)
  (if (single body)
      `(scheme-eval ',(first body) ,env #'values)
      `(scheme-eval '(begin ,@body) ,env #'values)))

#|
(let ((env (make-new-frame *primitive-environment*)))
  (setf (lookup '- env) #'-
        (lookup '* env) #'*
        (lookup 'zerop env) (lambda (x) (if (zerop x) 'true 'false)))
  (with-scheme env
    (define (fact n)
      (if (zerop n)
          1
          (* n (fact (- n 1))))))
  (time (loop :repeat 1e5
              :do (with-scheme env
                    (fact 1000)))))
|#
