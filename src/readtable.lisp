(defpackage scm-interp.readtable
  (:use :cl
        :scm-interp.util
        :scm-interp.core)
  (:export #:scheme-readtable-on
           #:scheme-readtable-off))
(in-package :scm-interp.readtable)


;;; readtable
(defun scheme-readtable-on ()
  (set-dispatch-macro-character
    #\# #\t
    (lambda (s c1 c2)
      (declare (ignore s c1 c2))
      ''true))
  (set-dispatch-macro-character
    #\# #\f
    (lambda (s c1 c2)
      (declare (ignore s c1 c2))
      ''false))
  (set-macro-character
    #\`
    (lambda (s c)
      (declare (ignore c))
      `'(quasiquote ,(read s t nil t))))
  (set-macro-character
    #\,
    (lambda (s c)
      (declare (ignore c))
      (let ((splice-p (eql #\@ (peek-char nil s t nil t))))
        (if splice-p
            (progn (read-char s t nil t)
                   `(unquote-splicing ,(read s t nil t)))
            `(unquote ,(read s t nil t)))))))

(defun scheme-readtable-off ()
  (setf *readtable* (copy-readtable nil)))
