(defpackage scm-interp.util
  (:use :cl)
  (:import-from :alexandria
                #:with-gensyms)
  (:export #:proper-list-p
           #:dotted-list-p
           #:single
           #:double
           #:compose
           #:n-times-compose
           #:partial
           #:rpartial
           #:alambda))
(in-package :scm-interp.util)


;;; list types
(defun proper-list-p (x)
  (or (null x)
      (and (consp x)
           (proper-list-p (cdr x)))))

(defun dotted-list-p (x)
  (and (consp x)
       (not (null (cdr x)))
       (or (atom (cdr x))
           (dotted-list-p (cdr x)))))


;;; list manipulation
(defun single (x)
  (and (consp x)
       (null (cdr x))))

(defun double (x)
  (and (consp x)
       (single (cdr x))))


;;; function manipulation
(defun compose (&rest fns)
  (flet ((cmp2 (g f)
           (lambda (&rest args)
             (funcall g (apply f args)))))
    (reduce #'cmp2 fns
            :initial-value #'values)))

(defun n-times-compose (n fn)
  (apply #'compose (make-list n :initial-element fn)))

(defun partial (fn &rest args)
  (lambda (&rest rest-args) (apply fn (append args rest-args))))

(defun rpartial (fn &rest args)
  (lambda (&rest rest-args) (apply fn (reverse (append args rest-args)))))


;;; closure utility
(defmacro closure (bindings args &body body)
  `(let ,bindings
     (lambda ,args ,@body)))

(defmacro define-closure-template (name initargs bindings args &body body)
  `(defun ,name ,initargs
     (closure ,bindings ,args ,@body)))


;;; anaphoric macro
(defmacro alambda (args &body body)
  `(labels ((self ,args ,@body))
     #'self))
