(defpackage scm-interp.syntax
  (:use :cl
        :scm-interp.util
        :scm-interp.core
        :scm-interp.environment)

  (:import-from :optima
                #:match)

  (:import-from :alexandria
                #:with-gensyms)

  (:export #:quote
           #:if
           #:begin
           #:lambda
           #:define
           #:set!

           #:cond
           #:case
           #:and
           #:or

           #:let
           #:let*
           #:letrec
           #:letrec*

           #:delay

           #:eqv?))
(in-package :scm-interp.syntax)


;;; special-forms
(define-scheme-syntax quote ((datum) env cont)
  (funcall cont datum))

(define-scheme-syntax if ((test then else) env cont)
  (declare (optimize speed))
  (if (not (eq (scheme-eval test env #'values) 'false))
      (scheme-eval then env cont)
      (scheme-eval else env cont)))

(define-scheme-syntax begin ((&rest exprs) env cont)
  (declare (optimize speed)
           (type function cont))
  (cond ((null exprs)
         (funcall cont 'false))
        ((single exprs)
         (scheme-eval (first exprs) env cont))
        (t
         (scheme-eval (first exprs) env cont)
         (scheme-eval `(begin ,@(rest exprs)) env cont))))

(define-scheme-syntax lambda ((fomals &rest body) env cont)
  (declare (optimize speed))
  (let ((cl-lambda-list
          (cond ((null fomals)
                 '())
                ((atom fomals)
                 `(&rest ,fomals))
                ((proper-list-p fomals)
                 fomals)
                ((dotted-list-p fomals)
                 `(,@(butlast fomals) ,(car (last fomals)) &rest ,(cdr (last fomals))))
                (t
                 (error 'scheme-lambda-fomals-error
                        :fomals fomals))))
        (new-env (gensym "NEW-ENV")))

    (scheme-eval (eval
                   `(lambda ,cl-lambda-list
                      (let ((,new-env (make-new-frame ',env)))
                        ,@(mapcar
                            (lambda (arg)
                              `(setf (lookup ',arg ,new-env)
                                     ,arg))
                            (remove '&rest cl-lambda-list))
                        (scheme-eval '(begin ,@body)
                                     ,new-env
                                     #'values))))
                 env
                 cont)))

(define-scheme-syntax define ((var &rest expr) env cont)
  (cond ((and (atom var) (single expr))
         (setf (lookup var env)
               (scheme-eval (first expr) env #'values))
         var)
        ((consp var)
         (scheme-eval `(define ,(car var) (lambda ,(cdr var) (begin ,@expr)))
                      env
                      cont))
        (t
         (error 'scheme-arity-error
                :expected 1
                :given (length expr)))))

(define-scheme-syntax set! ((var expr) env cont)
  (unless (lookup var env)
    (error 'scheme-unbound-variable-error :datum var))
  (setf (lookup var env)
        (scheme-eval expr env #'values))
  (values (lookup var env)))


;;; primitive macroes
(define-scheme-macro cond (&rest clauses)
  (match clauses
    ((cons (cons 'else else) _)
     else)
    ((cons (list test then) rest-clauses)
     `(if ,test
          ,then
          (cond ,@rest-clauses)))
    ((cons (list test '=> then) rest-clauses)
     `(if ,test
          ,then
          (cond ,@rest-clauses)))
    ((cons (cons test then-body) rest-clauses)
     `(if ,test
          (begin ,@then-body)
          (cond ,@rest-clauses)))
    (() '<undefined>)))

(define-scheme-macro case (key &rest clauses)
  (let ((key-sym (gensym "KEY")))
    `(let ((,key-sym ,key))
       (cond ,@(mapcar (lambda (clause)
                         (match clause
                           ((cons 'else body)
                            `(else ,@body))
                           ((cons data body)
                            `((or ,@(mapcar (lambda (datum)
                                              `(eqv? ',datum ',key-sym))
                                            data))
                              ,@body))))
                       clauses)))))

(define-scheme-macro and (&rest tests)
  (cond ((null tests)
         'true)
        ((single tests)
         (first tests))
        (t (let ((test (gensym "AND")))
             `(let ((,test ,(first tests)))
                (if ,test
                    (and ,@(rest tests))
                    false))))))

(define-scheme-macro or (&rest tests)
  (if (null tests)
      'false
      (let ((test (gensym "OR")))
        `(let ((,test ,(first tests)))
           (if ,test
               ,test
               (or ,@(rest tests)))))))

(define-scheme-macro let (bindings &rest body)
  (if (symbolp bindings)
      (let ((name bindings)
            (bindings (first body))
            (body (rest body)))
        `(letrec ((,name (lambda ,(mapcar #'first bindings) ,@body)))
           (,name ,@(mapcar #'second bindings))))
      `((lambda ,(mapcar #'first bindings) ,@body)
        ,@(mapcar #'second bindings))))

(define-scheme-macro let* (bindings &rest body)
  (if (null bindings)
      `(begin ,@body)
      `(let (,(first bindings))
         (let* (,@(rest bindings))
           ,@body))))

(define-scheme-macro letrec (bindings &rest body)
  `((lambda ()
      ,@(mapcar (lambda (binding) `(define ,(first binding) '<undefined>))
                bindings)
      ,@(mapcar (lambda (binding) `(set! ,(first binding) ,(second binding)))
                bindings)
      ,@body)))

(define-scheme-macro letrec* (bindings &rest body)
  (if (null bindings)
      `(begin ,@body)
      `(letrec (,(first bindings))
         (letrec* (,@(rest bindings)) ,@body))))

(define-scheme-macro delay (&rest body)
  (with-gensyms (cache forced? thunk)
    `(let ((,cache false)
           (,forced? false)
           (,thunk (lambda () ,@body)))
       (lambda ()
         (if ,forced?
             ,cache
             (begin (set! ,forced? true)
                    (set! ,cache (,thunk))
                    ,cache))))))

(let ((env (make-new-frame *primitive-environment*)))
  (setf (lookup '+ env) #'+
        (lookup '- env) #'-
        (lookup '< env) (lambda (x y) (if (< x y) 'true 'false)))
  (with-scheme env
    (define (fib n)
      (if (< n 2)
          1
          (+ (fib (- n 1)) (fib (- n 2))))))
  (time (with-scheme env
          (fib 38))))
(with-scheme (make-new-frame *primitive-environment*)
  (define (fib n)
    (if (< n 2)
        1
        (+ (fib (- n 1)) (fib (- n 2)))))
  (fib 3))
