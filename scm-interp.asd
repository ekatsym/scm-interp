(defsystem "scm-interp"
  :version "0.1.0"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("alexandria"
               "optima")
  :components ((:module "src"
                :serial t
                :components
                ((:file "util")
                 (:file "alist")
                 (:file "environment")
                 (:file "core")
                 (:file "syntax")
                 (:file "readtable"))))
  :description ""
  :in-order-to ((test-op (test-op "scm-interp/tests"))))

(defsystem "scm-interp/tests"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :depends-on ("scm-interp"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for scm-interp-as-macro"
  :perform (test-op (op c) (symbol-call :rove :run c)))
