(defpackage r5rs-as-macro/tests/main
  (:use :cl
        :r5rs-as-macro
        :rove))
(in-package :r5rs-as-macro/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :r5rs-as-macro)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
